package co.sca.dao;

import org.springframework.data.repository.CrudRepository;

import co.sca.model.Ticket;

//En CrudRepository el primer parámetro es type, entonces se ingresa la clase
//El parámetro ID indica el tipo del id de la entidad
public interface TicketDao extends CrudRepository<Ticket, Integer> {

}
