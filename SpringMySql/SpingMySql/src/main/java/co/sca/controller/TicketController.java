package co.sca.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.sca.dao.TicketDao;
import co.sca.model.Ticket;

//Incluye las anotaciones @Controller y @ResponseBody
//Se usa en conjunto con @RequestMapping
@RestController //Sirve para crear restful controllers.
@RequestMapping("/ticket") //Es por la url que se hace petición
public class TicketController {
	@Autowired //Esta es para la inyección de dependencias. Lo que se busca es no tener que instanciar cada clase
	private TicketDao dao;
	
	@PostMapping("/bookTickets") //Url por la que se hace una petición post
	public String bookTicket(@RequestBody List<Ticket> tickets) {
		dao.saveAll(tickets);
		return "ticket booked : " + tickets.size();
	}
	
	@GetMapping("/getTickets") //url para hacer peticiones get
	public List<Ticket> getTickets() {
		return (List<Ticket>) dao.findAll();
	}
}
