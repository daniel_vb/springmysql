package co.sca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpingMySqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpingMySqlApplication.class, args);
	}

}
