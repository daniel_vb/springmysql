package co.sca.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity //Indica que es una entidad de JPA (De la BD)
@Table(name = "Ticket") //Se usa para mapear a la tabla a la que se está haciendo referencia. name indica el nombre exacto de la tabla en la BD
@Getter //Genera el getter automáticamente
@Setter //Genera el setter automáticamente
@NoArgsConstructor //Constructor sin argumentos
@AllArgsConstructor //Constructor autogenerado con todos los argumentos
@ToString //Es una implementación del método ToString()
public class Ticket {
	@Id //Es para que JPA lo reconozca como el id
	@GeneratedValue //Es para indicar que el id se genera de forma automática.
	private int id;
	private double amount;
	private String category;
}
